# HKDM - HotKey Daemon (for) Mobile

HKDM is designed to be a modern replacement for [triggerhappy](https://github.com/wertarbyte/triggerhappy). It is written in rust and uses toml files for configuration. It is not a full replacement, but implements a minimal set of features, MRs welcome :D

HKDM is a simple hotkey daemon, it runs in the background and reacts to particular key events / combinations of events by running arbitrary commands.

It is designed for use in embedded applications, for example in [ttyescape](https://gitlab.com/postmarketOS/pmaports/-/tree/master/main/ttyescape), or embedded into a ramdisk for off-mode charging on a phone, where it can be used to toggle the display on/off.

HKDM doesn't support adopting new input devices yet, on phones it's expected that the key usecase is for voluem/power button key combos, these input devices have usually probed by the time hkdm is run.

## Config file format

Config files go in `/etc/hkdm/conf.d/` and are `.toml` files.

A list of valid keycodes can be found here: https://github.com/torvalds/linux/blob/master/include/uapi/linux/input-event-codes.h (or see `/usr/include/linux/input-event-codes.h if you have linux-headers installed).

A config file consists of a list of events to be bound, e.g.

```toml
[[events]]
# Name of the event (optional)
name = "increment"
# The type of the event
event_type = "EV_KEY"
# Only valid for EV_KEY events, should the command be fired on a press or release?
key_state = "released"
# The ordering of the keys here matters, all but the last key are treated like
# "modifier" keys, the command will only be run if they are pressed.
# The last key is the "action" key, the command will fire when it matches
# keys_state. In this case holding the volume down button and releasing 
# the power button will cause the command to be run
keys = ["KEY_VOLUMEDOWN", "KEY_POWER"]
command = "/usr/bin/toggltvt.sh inc"

[[events]]
name = "start"
event_type = "EV_KEY"
# here, pressing the volume down button will cause the command to be run
key_state = "pressed"
keys = ["KEY_VOLUMEDOWN"]
command = "/usr/bin/toggltvt.sh start"

[[events]]
name = "reset"
event_type = "EV_KEY"
# And here, releasing it causes the command to be run
key_state = "released"
keys = ["KEY_VOLUMEDOWN"]
command = "/usr/bin/toggltvt.sh reset"
```

The above config file sequences the following:

```
Executing trigger action: /usr/bin/togglevt.sh start
Executing trigger action: /usr/bin/togglevt.sh inc
Executing trigger action: /usr/bin/togglevt.sh inc
Executing trigger action: /usr/bin/togglevt.sh inc
Executing trigger action: /usr/bin/togglevt.sh reset
```

When volume down is pressed the "start" command is issued, pressing power whilst continuining to hold the volume down button issues "inc" commands, and finally releasing the volume down button issues "reset".

## Building

HKDM only depends on libevdev and cargo (rust).

Alpine:
```sh
sudo apk add libevdev-dev cargo
git clone https://gitlab.com/calebccff/hkdm
cd hkdm
cargo build
```

## Running (testing)

See `hkdm -h` for usage.

For TTYEscape:

```
./target/debug/hkdm -i -c ttyescape.toml
```